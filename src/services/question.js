import axios from "axios";
import { API_URL } from "../utils/config";
import request from "../utils/request";
export const getListQuestion = async (options) => {
  // return request().get("/admin/question", { params: { ...options } });
  return await axios({
    method: "GET",
    url: `${API_URL}/admin/question`,
    params: {
      ...options,
    },
  });
};

export const signS3Url = async (options) => {
  // return request().get("/admin/question", { params: { ...options } });
  return await axios({
    method: "GET",
    url: `${API_URL}/admin/s3/signed-url`,
    params: {
      ...options,
    },
  });
};
