import axios from "axios";
import { API_URL } from "../utils/config";
export const getListPracticePackage = async (options) => {
  // return request().get("/admin/question", { params: { ...options } });
  return await axios({
    method: "GET",
    url: `${API_URL}/admin/package`,
    params: {
      ...options,
    },
  });
};
