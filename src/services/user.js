import axios from "axios";
import { API_URL } from "../utils/config";
export const getListUser = async (options) => {
  return await axios({
    method: "GET",
    url: `${API_URL}/admin/user`,
    params: {
      ...options,
    },
  });
};
