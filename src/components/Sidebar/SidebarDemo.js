import {
  FileOutlined,
  PieChartOutlined,
  ExclamationCircleOutlined,
  TeamOutlined,
  UserOutlined,
  FlagOutlined,
  FolderAddOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import "antd/dist/antd.css";
import React from "react";
import { Link } from "react-router-dom";
import logo from "./logo.svg";
import "./Sidebar.css";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class SiderDemo extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  };

  render() {
    const { collapsed } = this.state;
    return (
      <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
        <div style={{ padding: "15px 0" }}>
          <img src={logo} alt="logo"></img>
        </div>
        <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
          {/* <Menu.Item key="1" icon={<PieChartOutlined />}>
            <Link to="/"> Dashboard</Link>
          </Menu.Item> */}
          <Menu.Item key="2" icon={<UserOutlined />}>
            <Link to="/users"> Manage Users</Link>
          </Menu.Item>

          <Menu.Item key="3" icon={<QuestionCircleOutlined />}>
            <Link to="/questions"> Manage Questions</Link>
          </Menu.Item>
          <Menu.Item key="4" icon={<FileOutlined />}>
            <Link to="/packages"> Manage Packages</Link>
          </Menu.Item>
          <Menu.Item key="5" icon={<FolderAddOutlined />}>
            <Link to="/toeictest"> Manage Toeic Test</Link>{" "}
          </Menu.Item>
          <Menu.Item key="6" icon={<FlagOutlined />}>
            <Link to="/report"> Manage Report</Link>
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

export default SiderDemo;
