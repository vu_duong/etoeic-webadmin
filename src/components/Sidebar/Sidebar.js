import React from "react";
import SidebarItem from "../../common/SidebarItem/SidebarItem";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCoffee } from "@fortawesome/free-solid-svg-icons";
import "./Sidebar.css";

function Sidebar() {
  return (
    <div className='sidebar'>
      <div>
        <h1 className='project-title'>E-TOEIC</h1>
      </div>
      <div>
        <SidebarItem
          link='/dashboard'
          leftIcon={<FontAwesomeIcon icon={faCoffee} />}
          rightTitle='Dashboard'
        />
        <SidebarItem
          link='/users'
          leftIcon={<FontAwesomeIcon icon={faCoffee} />}
          rightTitle='Manage Users'
        />
        <SidebarItem
          link='/questions'
          leftIcon={<FontAwesomeIcon icon={faCoffee} />}
          rightTitle='Question'
        />

        <SidebarItem
          link='/battles'
          leftIcon={<FontAwesomeIcon icon={faCoffee} />}
          rightTitle='Battles'
        />
      </div>
    </div>
  );
}

export default Sidebar;
