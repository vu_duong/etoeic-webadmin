import { Dropdown, Menu } from "antd";
import "antd/dist/antd.css";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import avatar from "../../assets/images/default-avatar.png";
import request from "../../utils/request";

const DropdownUser = () => {
  const history = useHistory();

  const [user, setUser] = useState({});
  const getCurrentUser = async () => {
    const usera = await request().get("/me");
    setUser(usera.data);
  };
  useEffect(() => {
    getCurrentUser();
  }, []);

  const onLogout = async () => {
    await request().post("/logout");
  };

  const onClickLogout = () => {
    onLogout();
    history.push("/login");
  };

  const menu = (
    <Menu>
      <Menu.Item key="0">
        <div onClick={onClickLogout}>Logout</div>
      </Menu.Item>
    </Menu>
  );

  return (
    <Dropdown overlay={menu} trigger={["click"]}>
      <div
        className="ant-dropdown-link"
        onClick={(e) => e.preventDefault()}
        style={{ cursor: "pointer" }}
      >
        Welcome, {user.displayName}
        <img
          src={avatar}
          alt="avatar"
          style={{
            width: 40,
            height: 40,
            borderRadius: "50%",
            marginLeft: "10px",
          }}
        />
      </div>
    </Dropdown>
  );
};

export default DropdownUser;
