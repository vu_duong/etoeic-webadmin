import { Input, Select } from "antd";
import { Option } from "antd/lib/mentions";
import React from "react";

const answers = [
  {
    id: "A",
    name: "A",
  },
  {
    id: "B",
    name: "B",
  },
  {
    id: "C",
    name: "C",
  },
  {
    id: "D",
    name: "D",
  },
];

const QuestionPart5 = (props) => {
  const {
    questionMemo,
    answerA,
    answerB,
    answerC,
    answerD,
    correctAnswer,
    guidance,
    handleChange,
    handleOnChangeAnswerQuizz1,
  } = props;

  const handleOnChange = (e) => {
    handleChange({ name: [e.target.name], value: e.target.value });
  };

  return (
    <>
      <tr>
        <td className="attribute-form">
          <span>Question memo</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="questionMemo"
            type="text"
            value={questionMemo}
            required
          />
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Answer A</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerA"
            type="text"
            value={answerA}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer B</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerB"
            type="text"
            value={answerB}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer C</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerC"
            type="text"
            value={answerC}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer D</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerD"
            type="text"
            value={answerD}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Correct Quiz 1</span>
        </td>
        <td>
          <Select
            value={correctAnswer}
            style={{ width: 120 }}
            onChange={handleOnChangeAnswerQuizz1}
          >
            {answers.map((ele, index) => {
              return (
                <Option key={index} value={ele.id}>
                  {ele.name}
                </Option>
              );
            })}
          </Select>
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Guidance</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="guidance"
            type="text"
            value={guidance}
          />
        </td>
      </tr>
    </>
  );
};

export default QuestionPart5;
