import React, { useState } from "react";
import styled from "styled-components";
import { Input } from "antd";
import { Upload, Button, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";

function AudioQuestion(props) {
  // console.log(props);
  const {
    questionMemo,
    questionAudio,
    questionImage,
    answerA,
    answerB,
    answerC,
    answerD,
    correctAnswer,
    guidance,
    handleChange,
  } = props;

  const handleOnsubmit = () => {};
  const handleOnChange = (e) => {
    handleChange({ name: [e.target.name], value: e.target.value });
  };

  const [fileList, updateFileList] = useState([]);
  const propsUpload = {
    action: "//jsonplaceholder.typicode.com/posts/",
    listType: "picture",
    previewFile(file) {
      console.log("Your upload file:", file);
      // Your process logic. Here we just mock to the same file
      return fetch("https://next.json-generator.com/api/json/get/4ytyBoLK8", {
        method: "POST",
        body: file,
      })
        .then((res) => res.json())
        .then(({ thumbnail }) => thumbnail);
    },
  };

  return (
    <>
      <tr>
        <td className='attribute-form'>
          <span>Question memo</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=''
            name='questionMemo'
            type='text'
            value={questionMemo}
          />
        </td>
      </tr>
      <tr>
        <td className='attribute-form'>
          <span>Answer A</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=''
            name='answerA'
            type='text'
            value={answerA}
          />
        </td>
      </tr>
      <tr>
        <td className='attribute-form'>
          <span>Image</span>
        </td>
        <td>
          <Upload {...propsUpload}>
            <Button icon={<UploadOutlined />}>Upload audio only</Button>
          </Upload>
        </td>
      </tr>
      <tr>
        <td className='attribute-form'>
          <span>Answer B</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=''
            name='answerB'
            type='text'
            value={answerB}
          />
        </td>
      </tr>
      <tr>
        <td className='attribute-form'>
          <span>Answer C</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=''
            name='answerC'
            type='text'
            value={answerC}
          />
        </td>
      </tr>
      <tr>
        <td className='attribute-form'>
          <span>Answer D</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=''
            name='answerD'
            type='text'
            value={answerD}
          />
        </td>
      </tr>
    </>
  );
}

export default AudioQuestion;
