import { PlusOutlined } from "@ant-design/icons";
import { Progress, Upload } from "antd";
import "antd/dist/antd.css";
import axios from "axios";
import Compressor from "compressorjs";
import React, { useState } from "react";
import { signS3Url } from "../../services/question";

const TestUpload = () => {
  const [defaultFileList, setDefaultFileList] = useState([]);
  const [progress, setProgress] = useState(0);

  const transformFile = (file) => {
    return new Promise((resolve, reject) => {
      new Compressor(file, {
        quality: 1,
        // maxWidth: 240,
        // maxHeight: 240,
        convertSize: 0,
        success(result) {
          resolve(result);
        },
        error(err) {
          reject(err);
        },
      });
    });
  };

  const uploadImage = async (options) => {
    const { onSuccess, onError, file, onProgress } = options;

    const newName = `${new Date().getTime()}-${file.name}`;
    const newFile = await transformFile(file);

    const queryParams = { name: newName, contentType: file.type };

    const resa = await signS3Url(queryParams);
    const urlToUpload = resa.data.url;

    try {
      await axios.put(urlToUpload, newFile, {
        headers: {
          "Content-Type": file.type,
        },
        onUploadProgress: (event) => {
          const percent = Math.floor((event.loaded / event.total) * 100);
          setProgress(percent);
          if (percent === 100) {
            setTimeout(() => setProgress(0), 1000);
          }
          onProgress({ percent: (event.loaded / event.total) * 100 });
        },
      });
      onSuccess(urlToUpload.split("?")[0], file);
    } catch (error) {
      onError(error);
    }

    // console.log("file", file);
    // console.log(file.type);
    // const config = {
    //   headers: { "Content-type": "image/png" },
    // onUploadProgress: (event) => {
    //   const percent = Math.floor((event.loaded / event.total) * 100);
    //   setProgress(percent);
    //   if (percent === 100) {
    //     setTimeout(() => setProgress(0), 1000);
    //   }
    //   onProgress({ percent: (event.loaded / event.total) * 100 });
    // },
    // };
    // fmData.append("image", file);

    // console.log(resa);
    // try {
    //   const res = await axios.put(resa.data.url, fmData, config);

    //   onSuccess("Ok");
    //   console.log("server res: ", res);
    // } catch (err) {
    //   console.log("Eroor: ", err);
    //   const error = new Error("Some error");
    //   onError({ err });
    // }
  };

  const handleOnChangeImg = ({ file, fileList, event }) => {
    console.log(file);
    //Using Hooks to update the state to the current filelist
    setDefaultFileList(file);
    //filelist - [{uid: "-1",url:'Some url to image'}]
  };

  return (
    <div class="container">
      <Upload
        name="file"
        accept="image/*"
        customRequest={uploadImage}
        onChange={handleOnChangeImg}
        listType="picture-card"
        defaultFileList={defaultFileList}
      >
        {defaultFileList.length >= 1 ? null : (
          <div>
            <PlusOutlined /> <div style={{ marginTop: 8 }}>Upload</div>
          </div>
        )}
      </Upload>
      {progress > 0 ? <Progress percent={progress} /> : null}
    </div>
  );
};

export default TestUpload;
