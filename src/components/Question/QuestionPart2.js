import { PlusOutlined, UploadOutlined } from "@ant-design/icons";
import { Button, Input, Progress, Select, Upload } from "antd";
import { Option } from "antd/lib/mentions";
import axios from "axios";
import React, { useState } from "react";
import { signS3Url } from "../../services/question";

const answers = [
  {
    id: "A",
    name: "A",
  },
  {
    id: "B",
    name: "B",
  },
  {
    id: "C",
    name: "C",
  },
  {
    id: "D",
    name: "D",
  },
];

const QuestionPart2 = (props) => {
  const {
    questionMemo,
    answerA,
    answerB,
    answerC,
    answerD,
    correctAnswer,
    guidance,
    handleChange,
    handleOnChangeAnswerQuizz1,
  } = props;

  const handleOnChange = (e) => {
    handleChange({ name: [e.target.name], value: e.target.value });
  };

  const [progressAudio, setProgressAudio] = useState(0);

  const [defaultAudioList, setDefaultAudioList] = useState([]);

  const uploadAudio = async (options) => {
    const { onSuccess, onError, file, onProgress } = options;

    const newName = `${new Date().getTime()}-${file.name}`;
    // const newFile = await transformFile(file);

    const queryParams = { name: newName, contentType: file.type };

    const resa = await signS3Url(queryParams);
    const urlToUpload = resa.data.url;

    try {
      await axios.put(urlToUpload, file, {
        headers: {
          "Content-Type": file.type,
        },
        onUploadProgress: (event) => {
          const percent = Math.floor((event.loaded / event.total) * 100);
          setProgressAudio(percent);
          if (percent === 100) {
            setTimeout(() => setProgressAudio(0), 1000);
          }
          onProgress({ percent: (event.loaded / event.total) * 100 });
        },
      });
      onSuccess(urlToUpload.split("?")[0], file);
    } catch (error) {
      onError(error);
    }
  };

  const handleOnChangeAudio = (file, event) => {
    handleChange({ name: "questionAudio", value: file.file.response });
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  return (
    <>
      <tr>
        <td className="attribute-form">
          <span>Question memo</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="questionMemo"
            type="text"
            value={questionMemo}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Question Audio</span>
        </td>
        <td>
          <Upload
            name="file"
            accept="audio/mp3"
            customRequest={uploadAudio}
            onChange={handleOnChangeAudio}
            defaultFileList={defaultAudioList}
          >
            <Button icon={<UploadOutlined />}>Upload audio only</Button>
          </Upload>
          {progressAudio > 0 ? <Progress percent={progressAudio} /> : null}
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Answer A</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerA"
            type="text"
            value={answerA}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer B</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerB"
            type="text"
            value={answerB}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer C</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerC"
            type="text"
            value={answerC}
            required
          />
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Correct Quiz 1</span>
        </td>
        <td>
          <Select
            value={correctAnswer}
            style={{ width: 120 }}
            onChange={handleOnChangeAnswerQuizz1}
          >
            {answers.map((ele, index) => {
              return (
                <Option key={index} value={ele.id}>
                  {ele.name}
                </Option>
              );
            })}
          </Select>
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Guidance</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="guidance"
            type="text"
            value={guidance}
          />
        </td>
      </tr>
    </>
  );
};

export default QuestionPart2;
