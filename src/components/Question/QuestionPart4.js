import { PlusOutlined, UploadOutlined } from "@ant-design/icons";
import { Button, Input, Progress, Select, Upload } from "antd";
import { Option } from "antd/lib/mentions";
import axios from "axios";
import Compressor from "compressorjs";
import * as _ from "lodash";
import React, { useState } from "react";
import { signS3Url } from "../../services/question";

const answers = [
  {
    id: "A",
    name: "A",
  },
  {
    id: "B",
    name: "B",
  },
  {
    id: "C",
    name: "C",
  },
  {
    id: "D",
    name: "D",
  },
];

const QuestionPart4 = (props) => {
  const {
    script,
    questionMemo,
    answerA,
    answerB,
    answerC,
    answerD,
    correctAnswer,
    question2Memo,
    answer2A,
    answer2B,
    answer2C,
    answer2D,
    correct2Answer,
    question3Memo,
    answer3A,
    answer3B,
    answer3C,
    answer3D,
    correct3Answer,
    handleOnChangeAnswerQuizz1,
    handleOnChangeAnswerQuizz2,
    handleOnChangeAnswerQuizz3,
    guidance,
    handleChange,
  } = props;

  const handleOnChange = (e) => {
    handleChange({ name: [e.target.name], value: e.target.value });
  };

  const [progressImg, setProgressImg] = useState(0);
  const [progressAudio, setProgressAudio] = useState(0);

  const [defaultImgList, setDefaultImgList] = useState([]);
  const [defaultAudioList, setDefaultAudioList] = useState([]);

  const [imgUrl, setImgUrl] = useState("");

  const transformFile = (file) => {
    return new Promise((resolve, reject) => {
      new Compressor(file, {
        quality: 1,
        convertSize: 0,
        success(result) {
          resolve(result);
        },
        error(err) {
          reject(err);
        },
      });
    });
  };

  const uploadImage = async (options) => {
    const { onSuccess, onError, file, onProgress } = options;

    const newName = `${new Date().getTime()}-${file.name}`;
    const newFile = await transformFile(file);

    const queryParams = { name: newName, contentType: file.type };

    const resa = await signS3Url(queryParams);
    const urlToUpload = resa.data.url;

    try {
      await axios.put(urlToUpload, newFile, {
        headers: {
          "Content-Type": file.type,
        },
        onUploadProgress: (event) => {
          const percent = Math.floor((event.loaded / event.total) * 100);
          setProgressImg(percent);
          if (percent === 100) {
            setTimeout(() => setProgressImg(0), 1000);
          }
          onProgress({ percent: (event.loaded / event.total) * 100 });
        },
      });
      onSuccess(urlToUpload.split("?")[0], file);
    } catch (error) {
      onError(error);
    }
  };

  const handleOnChangeImage = (file, event) => {
    setImgUrl(file.file.response);
    handleChange({ name: "questionImage", value: file.file.response });
  };

  const uploadAudio = async (options) => {
    const { onSuccess, onError, file, onProgress } = options;

    const newName = `${new Date().getTime()}-${file.name}`;
    // const newFile = await transformFile(file);

    const queryParams = { name: newName, contentType: file.type };

    const resa = await signS3Url(queryParams);
    const urlToUpload = resa.data.url;

    try {
      await axios.put(urlToUpload, file, {
        headers: {
          "Content-Type": file.type,
        },
        onUploadProgress: (event) => {
          const percent = Math.floor((event.loaded / event.total) * 100);
          setProgressAudio(percent);
          if (percent === 100) {
            setTimeout(() => setProgressAudio(0), 1000);
          }
          onProgress({ percent: (event.loaded / event.total) * 100 });
        },
      });
      onSuccess(urlToUpload.split("?")[0], file);
    } catch (error) {
      onError(error);
    }
  };

  const handleOnChangeAudio = (file, event) => {
    handleChange({ name: "questionAudio", value: file.file.response });
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  return (
    <>
      <tr>
        <td className="attribute-form">
          <span>Question Audio</span>
        </td>
        <td>
          <Upload
            name="file"
            accept="audio/mp3"
            customRequest={uploadAudio}
            onChange={handleOnChangeAudio}
            defaultFileList={defaultAudioList}
          >
            <Button icon={<UploadOutlined />}>Upload audio only</Button>
          </Upload>
          {progressAudio > 0 ? <Progress percent={progressAudio} /> : null}
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Image</span>
        </td>
        <td>
          <Upload
            name="file"
            accept="image/*"
            customRequest={uploadImage}
            onChange={handleOnChangeImage}
            listType="picture-card"
            showUploadList={false}
            defaultFileList={defaultImgList}
          >
            {!_.isEmpty(imgUrl) ? (
              <img src={imgUrl} alt="avatar" style={{ width: "100%" }} />
            ) : (
              uploadButton
            )}
          </Upload>
          {progressImg > 0 ? <Progress percent={progressImg} /> : null}
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Script</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="script"
            type="text"
            value={script}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Question memo</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="questionMemo"
            type="text"
            value={questionMemo}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer A</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerA"
            type="text"
            value={answerA}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer B</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerB"
            type="text"
            value={answerB}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer C</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerC"
            type="text"
            value={answerC}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer D</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answerD"
            type="text"
            value={answerD}
            required
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Correct Quiz 1</span>
        </td>
        <td>
          <Select
            value={correctAnswer}
            style={{ width: 120 }}
            onChange={handleOnChangeAnswerQuizz1}
          >
            {answers.map((ele, index) => {
              return (
                <Option key={index} value={ele.id}>
                  {ele.name}
                </Option>
              );
            })}
          </Select>
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Question 2 memo</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="question2Memo"
            type="text"
            value={question2Memo}
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer 2A</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answer2A"
            type="text"
            value={answer2A}
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer 2 B</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answer2B"
            type="text"
            value={answer2B}
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer 2 C</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answer2C"
            type="text"
            value={answer2C}
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer 2 D</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answer2D"
            type="text"
            value={answer2D}
          />
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Correct Quizz 2</span>
        </td>
        <td>
          <Select
            value={correct2Answer}
            style={{ width: 120 }}
            onChange={handleOnChangeAnswerQuizz2}
          >
            {answers.map((ele, index) => {
              return (
                <Option key={index} value={ele.id}>
                  {ele.name}
                </Option>
              );
            })}
          </Select>
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Question 3 memo</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="question3Memo"
            type="text"
            value={question3Memo}
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer 3 A</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answer3A"
            type="text"
            value={answer3A}
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer 3 B</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answer3B"
            type="text"
            value={answer3B}
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer 3 C</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answer3C"
            type="text"
            value={answer3C}
          />
        </td>
      </tr>
      <tr>
        <td className="attribute-form">
          <span>Answer 3 D</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="answer3D"
            type="text"
            value={answer3D}
          />
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Correct quizz 3:</span>
        </td>
        <td>
          <Select
            value={correct3Answer}
            style={{ width: 120 }}
            onChange={handleOnChangeAnswerQuizz3}
          >
            {answers.map((ele, index) => {
              return (
                <Option key={index} value={ele.id}>
                  {ele.name}
                </Option>
              );
            })}
          </Select>
        </td>
      </tr>

      <tr>
        <td className="attribute-form">
          <span>Guidance</span>
        </td>
        <td>
          <Input.TextArea
            onChange={handleOnChange}
            autoSize
            placeholder=""
            name="guidance"
            type="text"
            value={guidance}
          />
        </td>
      </tr>
    </>
  );
};

export default QuestionPart4;
