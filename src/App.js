import { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import CustomLayout from "./screens/Layout";
import Login from "./screens/Login/Login";

function App() {
  return (
    <div className='App'>
      <Router>
        <Switch>
          <Route exact path='/login' component={Login} />
          <CustomLayout />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
