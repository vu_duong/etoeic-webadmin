import { FrownOutlined, SmileOutlined } from "@ant-design/icons";
import {
  Button,
  Form,
  Input,
  InputNumber,
  notification,
  Spin,
  Typography,
} from "antd";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import request from "../../utils/request";

const WrapStyled = styled.div`
  &&& {
    font-size: 14px;
    padding: 16px;
    .wrap-form {
      form {
        width: 800px;
        display: flex;
        flex-direction: column;
        & > div {
          margin-bottom: 16px;
        }
        .wrap-button {
          display: flex;
          justify-content: center;
          align-items: center;
          button {
            margin: 8px;
          }
        }
      }
    }
  }
`;

const layout = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 15,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const CreateUserScreen = (props) => {
  const history = useHistory();

  const [loading, setLoading] = useState(false);

  const handleOnCreateUser = async (value) => {
    console.log(value);

    setLoading(true);
    try {
      const body = {
        ...value,
      };
      await request().post("/admin/user", body);

      openNotification();
      setLoading(false);
      history.push("/users");
    } catch (error) {
      openError(error);
      setLoading(false);
    }
  };

  const onFinish = (value) => {
    handleOnCreateUser(value);
  };

  const openError = (error) => {
    notification.open({
      message: "FAILED!",
      description: "Something went wrong, please try again later" + error,
      icon: <FrownOutlined style={{ color: "#ff4d4f" }} />,
      duration: 5,
    });
  };

  const openNotification = () => {
    notification.open({
      message: "Successfully!",
      description: "Create new User successfully!",
      icon: <SmileOutlined style={{ color: "#108ee9" }} />,
      duration: 5,
    });
  };

  return (
    <WrapStyled>
      {loading && (
        <div className="loading">
          <Spin tip="Loading..." size="large"></Spin>
        </div>
      )}

      <Typography.Title level={3}>Create New User</Typography.Title>
      <Form
        {...layout}
        name="basic"
        onFinish={onFinish}
        // onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input a valid email!",
              type: "email",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please set password for user!" }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item label="Display name" name="displayName">
          <Input />
        </Form.Item>
        <Form.Item label="First name" name="firstName">
          <Input />
        </Form.Item>
        <Form.Item label="Last name" name="lastName">
          <Input />
        </Form.Item>

        <Form.Item label="Coin" name="coin" type="number" min={10}>
          <InputNumber min={10} />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button
            type="primary"
            style={{ marginRight: "10px" }}
            htmlType="submit"
          >
            Create
          </Button>
          <Button
            onClick={() => {
              history.push("/users");
            }}
          >
            Cancel
          </Button>
        </Form.Item>
      </Form>
    </WrapStyled>
  );
};

export default CreateUserScreen;
