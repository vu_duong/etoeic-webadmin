import { Button, Table, Typography } from "antd";
import Search from "antd/lib/input/Search";
import Modal from "antd/lib/modal/Modal";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import request from "../../utils/request";

const WrapDiv = styled.div`
  &&& {
    padding: 16px;
    .button-create {
      margin-bottom: 16px;
    }
  }
`;

const MangeUsersScreen = () => {
  const columns = [
    {
      title: "Id",
      key: "id",
      dataIndex: "id",
    },
    {
      title: "Username",
      key: "username",
      dataIndex: "username",
    },
    {
      title: "Email",
      key: "email",
      dataIndex: "email",
    },
    {
      title: "Name",
      key: "name",
      dataIndex: "name",
    },
    {
      title: "Coin",
      key: "coin",
      dataIndex: "coin",
    },
    {
      title: "Last login",
      key: "lastLogin",
      dataIndex: "lastLogin",
      render: (lastLogin) => {
        if (!lastLogin) {
          return <p>--</p>;
        }
        return <p>{moment(lastLogin).format("HH:mm DD/MM/yyyy")}</p>;
      },
    },
    {
      title: "",
      key: "action",
      dataIndex: "id",
      render: (id, record) => {
        return (
          <div
            style={{
              display: "flex",
            }}
          >
            <Button
              type="primary"
              style={{ marginRight: "4px" }}
              onClick={() => {
                history.push(`/users/${record.id}`);
              }}
            >
              Update
            </Button>

            {record.isDisable ? (
              <Button
                type="primary"
                onClick={() => {
                  setSelectedId(id);
                  setVisibleModal(true);
                }}
                style={{ background: "#0abc6f" }}
              >
                Enable
              </Button>
            ) : (
              <Button
                type="primary"
                danger
                onClick={() => {
                  setSelectedId(id);
                  setVisibleModal(true);
                }}
                // style={{ background: "#697988" }}
              >
                Disable
              </Button>
            )}
          </div>
        );
      },
    },
  ];
  const history = useHistory();

  const [loading, setLoading] = useState(false);
  const [usersData, setUsersData] = useState([]);

  const [search, setSearch] = useState("");

  const [visibleModal, setVisibleModal] = useState(false);
  const [selectedId, setSelectedId] = useState(0);

  const defaultPagination = {
    current: 1,
    pageSize: 5,
    total: 0,
  };

  const [pagination, setPagination] = useState(defaultPagination);

  const handleTableChange = (newPage, sortEvent) => {
    fetchListUser(newPage);
  };

  const convertUserForTable = (rawData) => {
    return rawData.users.map((x) => {
      const fname = x.firstName || "-";
      const lName = x.lastName || "-";
      return {
        id: x.id,
        username: x.username,
        email: x.email,
        name: fname + " " + lName,
        coin: x.totalCoin,
        lastLogin: x.lastLogin,
        action: "",
        isDisable: x.isDisable,
      };
    });
  };

  const fetchListUser = async (newPage) => {
    setLoading(true);
    const paramPagination = newPage || pagination;
    setPagination(paramPagination);
    const skip = (paramPagination.current - 1) * paramPagination.pageSize;
    const take = paramPagination.pageSize;

    const options = { search, skip, take };

    try {
      const users = await request().get("/admin/user", {
        params: { ...options },
      });
      const converted = convertUserForTable(users.data);
      setUsersData(converted);
      const newPagination = { ...paramPagination, total: users.data.total };
      setPagination(newPagination);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const toggleEnableUser = async () => {
    setLoading(true);

    await request().post(`/admin/user/enable/${selectedId}`);

    setVisibleModal(false);
    fetchListUser();
  };

  const hideModal = () => {
    setVisibleModal(false);
  };

  const onSearch = (value) => {
    console.log(value);
    setPagination(defaultPagination);
    setSearch(value);
  };

  useEffect(() => {
    fetchListUser();
  }, [search]);

  return (
    <WrapDiv>
      <Typography.Title level={3}>Users</Typography.Title>
      <div className="button-create">
        <Button
          type="primary"
          onClick={() => {
            history.push("users/new");
          }}
        >
          Create new User
        </Button>
      </div>

      <Search
        placeholder="Search by name, username, email"
        allowClear
        onSearch={onSearch}
        style={{ margin: "10px 0" }}
      />
      <Table
        columns={columns}
        dataSource={usersData}
        pagination={pagination}
        loading={loading}
        onChange={handleTableChange}
      ></Table>
      <Modal
        icon="ExclamationCircleOutlined"
        title="Confirm"
        visible={visibleModal}
        onOk={toggleEnableUser}
        onCancel={hideModal}
        okType="danger"
        okText="OK"
        cancelText="Cancel"
      >
        <p>Are you sure to disable this user?</p>
      </Modal>
    </WrapDiv>
  );
};

export default MangeUsersScreen;
