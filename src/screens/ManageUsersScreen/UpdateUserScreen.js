import { SmileOutlined, FrownOutlined } from "@ant-design/icons";
import {
  Button,
  Form,
  Input,
  InputNumber,
  notification,
  Spin,
  Switch,
  Typography,
} from "antd";
import * as _ from "lodash";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import request from "../../utils/request";

const WrapStyled = styled.div`
  &&& {
    font-size: 14px;
    padding: 16px;
    .wrap-form {
      form {
        width: 800px;
        display: flex;
        flex-direction: column;
        & > div {
          margin-bottom: 16px;
        }
        .wrap-button {
          display: flex;
          justify-content: center;
          align-items: center;
          button {
            margin: 8px;
          }
        }
      }
    }
  }
`;

const layout = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 15,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const UpdateUserScreen = (props) => {
  const [userData, setUserData] = useState({});

  const history = useHistory();

  const [loading, setLoading] = useState(false);

  const handleOnUpdateUser = async (value) => {
    setLoading(true);
    try {
      const body = {
        ...value,
      };
      await request().post("/admin/users/:id", body);

      openNotification();
      setLoading(false);
      history.push("/users");
    } catch (error) {
      setLoading(false);
      openError(error);
    }
  };

  const openNotification = () => {
    notification.open({
      message: "Successfully!",
      description: "Update User successfully!",
      icon: <SmileOutlined style={{ color: "#108ee9" }} />,
      duration: 5,
    });
  };

  const openError = (error) => {
    notification.open({
      message: "FAILED!",
      description: "Something went wrong, please try again later" + error,
      icon: <FrownOutlined style={{ color: "#ff4d4f" }} />,
      duration: 5,
    });
  };

  const onGetUserData = async (id) => {
    try {
      setLoading(true);
      const userData = await request().get(`/admin/user/${id}`);

      setUserData(userData.data);

      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    const id = props.match.params.id;
    onGetUserData(id);
  }, []);

  const onFinish = (value) => {
    handleOnUpdateUser(value);
  };

  return (
    <WrapStyled>
      {loading && (
        <div className="loading">
          <Spin tip="Loading..." size="large"></Spin>
        </div>
      )}
      <Typography.Title level={3}>Update User</Typography.Title>
      {!_.isEmpty(userData) && (
        <Form
          {...layout}
          name="basic"
          onFinish={onFinish}
          initialvalues={{ email: "hihihi", displayName: "kokok" }}
          // onFinishFailed={onFinishFailed}
        >
          <Form.Item label="Email">
            <Input name="email " disabled={true} value={userData.email} />
          </Form.Item>

          <Form.Item label="Display name" name="displayName">
            <Input defaultValue={userData.displayName} />
          </Form.Item>
          {/* 
          <Form.Item label="Display name" name="displayName">
            <Input />
          </Form.Item> */}

          <Form.Item label="First name" name="firstName">
            <Input defaultValue={userData.firstName} />
          </Form.Item>

          <Form.Item label="Last name" name="lastName">
            <Input defaultValue={userData.lastName} />
          </Form.Item>

          <Form.Item label="Coin" name="coin" type="number" min={10}>
            <InputNumber min={10} defaultValue={userData.totalCoin} />
          </Form.Item>

          <Form.Item name="isDisable" label="Disable User">
            <Switch
              checkedChildren="Enable"
              unCheckedChildren="Disable"
              defaultChecked={userData.isDisable}
            />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              style={{ marginRight: "10px" }}
              htmlType="submit"
            >
              Save
            </Button>
            <Button
              onClick={() => {
                history.push("/users");
              }}
            >
              Cancel
            </Button>
          </Form.Item>
        </Form>
      )}
    </WrapStyled>
  );
};

export default UpdateUserScreen;
