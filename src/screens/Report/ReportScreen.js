import { Button, Table, Tag, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import request from "../../utils/request";
import moment from "moment";

const WrapDiv = styled.div`
  &&& {
    padding: 16px;
    .button-create {
      margin-bottom: 16px;
    }
  }
`;

function ReportScreen() {
  const columns = [
    {
      title: "Question Id",
      key: "questionId",
      dataIndex: "questionId",
    },
    {
      title: "Category",
      key: "category",
      dataIndex: "category",
    },
    {
      title: "Email",
      key: "email",
      dataIndex: "email",
    },
    {
      title: "Name",
      key: "name",
      dataIndex: "name",
    },
    {
      title: "Submit",
      key: "submissionTime",
      dataIndex: "submissionTime",
      render: (submissionTime) => {
        if (!submissionTime) {
          return <p>--</p>;
        }
        return <p>{moment(submissionTime).format("HH:mm DD/MM/yyyy")}</p>;
      },
    },
    {
      title: "Type",
      key: "type",
      dataIndex: "type",
      render: (type) => {
        let color = "lime";
        switch (type) {
          case "PART1":
            color = "lime";
            break;
          case "PART2":
            color = "green";
            break;
          case "PART3":
            color = "cyan";
            break;
          case "PART4":
            color = "blue";
            break;
          case "PART5":
            color = "geekblue";
            break;
          case "PART6":
            color = "purple";
            break;
          case "PART7":
            color = "gold";
            break;
          default:
            color = "grlimeeen";
        }
        return <Tag color={color}>{type}</Tag>;
      },
    },

    {
      title: "",
      key: "action",
      dataIndex: "action",
      render: (id) => (
        <Button
          style={{ marginRight: "4px" }}
          onClick={() => {
            console.log(id);
            history.push(`questions/${id}`);
          }}
        >
          Review question
        </Button>
      ),
    },
  ];
  const history = useHistory();

  const [loading, setLoading] = useState(false);
  const [reportData, setReportData] = useState([]);

  const defaultPagination = {
    current: 1,
    pageSize: 5,
    total: 0,
  };

  const [pagination, setPagination] = useState(defaultPagination);

  const fetchListReport = async (newPage) => {
    setLoading(true);
    const paramPagination = newPage || pagination;
    setPagination(paramPagination);
    const skip = (paramPagination.current - 1) * paramPagination.pageSize;
    const take = paramPagination.pageSize;

    try {
      const rawReports = await request().get(
        `admin/report?skip=${skip}&take=${take}`
      );
      //   const rawReports = await getListPracticePackage(options);
      convertReportForTable(rawReports.data.data);

      const newPagination = {
        ...paramPagination,
        total: rawReports.data.total,
      };
      setPagination(newPagination);
    } catch (error) {
      setLoading(false);
    }
    setLoading(false);
  };

  const convertReportForTable = (rawData) => {
    const data = rawData.map((x) => {
      return {
        key: x.id,
        category: x.category,
        questionId: x.question.id,
        type: x.question.type,
        submissionTime: x.submissionTime,
        email: x.user.email,
        name: x.user.displayName,
        action: x.questionId,
        comment: x.comment,
      };
    });
    console.log(data);
    setReportData(data);
  };

  useEffect(() => {
    fetchListReport();
  }, []);

  const handleTableChange = (newPage, sortEvent) => {
    console.log(newPage, sortEvent);
    fetchListReport(newPage);
  };

  return (
    <WrapDiv>
      <Typography.Title level={3}>Report Question</Typography.Title>

      <Table
        columns={columns}
        dataSource={reportData}
        pagination={pagination}
        loading={loading}
        onChange={handleTableChange}
        expandable={{
          expandedRowRender: (record) => (
            <p style={{ margin: 0 }}>{record.comment}</p>
          ),
        }}
      ></Table>
    </WrapDiv>
  );
}

export default ReportScreen;
