import React, { useReducer } from "react";
import Button from "@material-ui/core/Button";
import styled from "styled-components";
import TextField from "@material-ui/core/TextField";
import { Typography } from "antd";
const WrapStyled = styled.div`
  &&& {
    font-size: 14px;
    padding: 16px;
    .wrap-form {
      form {
        width: 800px;
        display: flex;
        flex-direction: column;
        & > div {
          margin-bottom: 16px;
        }
        .wrap-button {
          display: flex;
          justify-content: center;
          align-items: center;
          button {
            margin: 8px;
          }
        }
      }
    }
  }
`;

function ShowUsersScreen(props) {
  console.log(props);
  const [state, setState] = useReducer(
    (preState, newState) => ({ ...preState, ...newState }),
    {
      name: "",
      email: "",
      password: "",
    }
  );

  const { name, email, password } = state;

  const handleOnsubmit = () => {};
  const handleOnChange = (e) => {
    setState({ [e.target.name]: e.target.value });
  };

  return (
    <WrapStyled>
      <Typography.Title level={3}>Update Question</Typography.Title>

      <div className="wrap-form">
        <form autoComplete="off" onSubmit={handleOnsubmit}>
          <TextField
            onChange={handleOnChange}
            name="name"
            label="Name"
            type="name"
            value={name}
          />
          <TextField
            onChange={handleOnChange}
            name="email"
            label="Email"
            type="email"
            value={email}
          />
          <TextField
            onChange={handleOnChange}
            name="password"
            label="Password"
            type="password"
            value={password}
          />
          <div className="wrap-button">
            <Button variant="contained" type="submit">
              Update
            </Button>
          </div>
        </form>
      </div>
    </WrapStyled>
  );
}

export default ShowUsersScreen;
