import { SmileOutlined, FrownOutlined } from "@ant-design/icons";

import { Button, notification, Select, Spin, Typography } from "antd";
import React, { useEffect, useReducer, useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import QuestionPart1 from "../../components/Question/QuestionPart1";
import QuestionPart2 from "../../components/Question/QuestionPart2";
import QuestionPart3 from "../../components/Question/QuestionPart3";
import QuestionPart4 from "../../components/Question/QuestionPart4";
import QuestionPart5 from "../../components/Question/QuestionPart5";
import QuestionPart6 from "../../components/Question/QuestionPart6";
import QuestionPart7 from "../../components/Question/QuestionPart7";

import request from "../../utils/request";

const { Option } = Select;

const WrapStyled = styled.div`
  &&& {
    font-size: 14px;
    padding: 16px;
    .wrap-form {
      form {
        width: 1200px;
        display: flex;
        font-size: 14px;
        flex-direction: column;
        & > div {
          margin-bottom: 16px;
        }
        .wrap-button {
          display: flex;
          justify-content: center;
          align-items: center;
          button {
            margin: 8px;
          }
        }
        .attribute-form {
          width: 200px;
          text-align: right;
          span {
            text-align: right;
            padding-right: 16px;
            font-size: 16px;
          }
        }
        table {
          tr {
            td {
              margin-bottom: 8px;
              padding-bottom: 8px;
            }
          }
        }
      }
    }
  }
`;

const types = [
  {
    id: "PART1",
    name: "Part 1",
  },
  {
    id: "PART2",
    name: "Part 2",
  },
  {
    id: "PART3",
    name: "Part 3",
  },
  {
    id: "PART4",
    name: "Part 4",
  },
  {
    id: "PART5",
    name: "Part 5",
  },
  {
    id: "PART6",
    name: "Part 6",
  },
  {
    id: "PART7",
    name: "Part 7",
  },
];

const initialState = {
  id: 0,
  type: "PART1",
  questionMemo: "",
  questionAudio: "",
  questionImage: "",
  answerA: "",
  answerB: "",
  answerC: "",
  answerD: "",
  correctAnswer: "A",
  correctAnswer2: "A",
  guidance: "",
};

function UpdateQuestionScreen(props) {
  const history = useHistory();
  const [loading, setLoading] = useState(false);

  const [state, setState] = useReducer(
    (preState, newState) => ({ ...preState, ...newState }),
    initialState
  );

  const {
    type,
    questionAudio,
    script,
    questionImage,
    questionMemo,
    answerA,
    answerB,
    answerC,
    answerD,
    correctAnswer,
    question2Memo,
    answer2A,
    answer2B,
    answer2C,
    answer2D,
    correct2Answer,
    question3Memo,
    answer3A,
    answer3B,
    answer3C,
    answer3D,
    correct3Answer,
    question4Memo,
    answer4A,
    answer4B,
    answer4C,
    answer4D,
    correct4Answer,
    question5Memo,
    answer5A,
    answer5B,
    answer5C,
    answer5D,
    correct5Answer,
    guidance,
  } = state;

  const handleOnsubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const res = await request().put("/admin/question", state, {
        params: { type: state.type },
      });
      openNotification();
      setLoading(false);
      history.push("/questions");
    } catch (error) {
      openError(error);
      setLoading(false);
    }
  };
  const handleChange = ({ name, value }) => {
    setState({ [name]: value });
  };

  const handleOnChangeType = (value) => {
    setState({ type: value });
  };

  const onGetQuestionData = async (id) => {
    const rawData = await request().get(`admin/question/${id}`);
    const question = rawData.data;
    setState({
      id: question.id,
      type: question.type,
      questionAudio: question.questionData.questionAudio,
      script: question.questionData.script,
      questionImage: question.questionData.questionImage,
      questionMemo: question.questionData.questionMemo,
      answerA: question.questionData.answerA,
      answerB: question.questionData.answerB,
      answerC: question.questionData.answerC,
      answerD: question.questionData.answerD,
      correctAnswer: question.questionData.correctAnswer,
      question2Memo: question.questionData.question2Memo,
      answer2A: question.questionData.answer2A,
      answer2B: question.questionData.answer2B,
      answer2C: question.questionData.answer2C,
      answer2D: question.questionData.answer2D,
      correct2Answer: question.questionData.correct2Answer,
      question3Memo: question.questionData.question3Memo,
      answer3A: question.questionData.answer3A,
      answer3B: question.questionData.answer3B,
      answer3C: question.questionData.answer3C,
      answer3D: question.questionData.answer3D,
      correct3Answer: question.questionData.correct3Answer,
      question4Memo: question.questionData.question4Memo,
      answer4A: question.questionData.answer4A,
      answer4B: question.questionData.answer4B,
      answer4C: question.questionData.answer4C,
      answer4D: question.questionData.answer4D,
      correct4Answer: question.questionData.correct4Answer,
      question5Memo: question.questionData.question5Memo,
      answer5A: question.questionData.answer5A,
      answer5B: question.questionData.answer5B,
      answer5C: question.questionData.answer5C,
      answer5D: question.questionData.answer5D,
      correct5Answer: question.questionData.correct5Answer,
      guidance: question.questionData.guidance,
    });
    console.log(rawData.data);
  };

  const openNotification = () => {
    notification.open({
      message: "Successfully!",
      description: "Update User successfully!",
      icon: <SmileOutlined style={{ color: "#108ee9" }} />,
      duration: 5,
    });
  };

  const openError = (error) => {
    notification.open({
      message: "FAILED!",
      description: "Something went wrong, please try again later" + error,
      icon: <FrownOutlined style={{ color: "#ff4d4f" }} />,
      duration: 5,
    });
  };

  useEffect(() => {
    const id = props.match.params.id;
    onGetQuestionData(id);
  }, []);

  return (
    <WrapStyled>
      {loading && (
        <div className="loading">
          <Spin tip="Loading..." size="large"></Spin>
        </div>
      )}

      <Typography.Title level={3}>Update Question</Typography.Title>
      <div className="wrap-form">
        <form autoComplete="off" onSubmit={handleOnsubmit}>
          <table>
            <tr>
              <td className="attribute-form">
                <span>Question Type:</span>
              </td>
              <td>
                <Select
                  value={type}
                  style={{ width: 120 }}
                  onChange={handleOnChangeType}
                  disabled={true}
                >
                  {types.map((ele, index) => {
                    return (
                      <Option key={index} value={ele.id}>
                        {ele.name}
                      </Option>
                    );
                  })}
                </Select>
              </td>
            </tr>
            {type === "PART1" && (
              <QuestionPart1
                questionMemo={questionMemo}
                answerA={answerA}
                answerB={answerB}
                answerC={answerC}
                answerD={answerD}
                questionAudio={questionAudio}
                questionImage={questionImage}
                correctAnswer={correctAnswer}
                guidance={guidance}
                handleChange={handleChange}
                handleOnChangeAnswerQuizz1={(value) => {
                  setState({ correctAnswer: value });
                }}
              />
            )}
            {type === "PART2" && (
              <QuestionPart2
                questionMemo={questionMemo}
                answerA={answerA}
                answerB={answerB}
                answerC={answerC}
                answerD={answerD}
                questionAudio={questionAudio}
                correctAnswer={correctAnswer}
                guidance={guidance}
                handleChange={handleChange}
                handleOnChangeAnswerQuizz1={(value) => {
                  setState({ correctAnswer: value });
                }}
              />
            )}
            {type === "PART3" && (
              <QuestionPart3
                script={script}
                questionMemo={questionMemo}
                questionAudio={questionAudio}
                questionImage={questionImage}
                answerA={answerA}
                answerB={answerB}
                answerC={answerC}
                answerD={answerD}
                correctAnswer={correctAnswer}
                question2Memo={question2Memo}
                answer2A={answer2A}
                answer2B={answer2B}
                answer2C={answer2C}
                answer2D={answer2D}
                question3Memo={question3Memo}
                answer3A={answer3A}
                answer3B={answer3B}
                answer3C={answer3C}
                answer3D={answer3D}
                guidance={guidance}
                handleChange={handleChange}
                handleOnChangeAnswerQuizz1={(value) => {
                  setState({ correctAnswer: value });
                }}
                handleOnChangeAnswerQuizz2={(value) => {
                  setState({ correct2Answer: value });
                }}
                handleOnChangeAnswerQuizz3={(value) => {
                  setState({ correct3Answer: value });
                }}
              />
            )}
            {type === "PART4" && (
              <QuestionPart4
                script={script}
                questionMemo={questionMemo}
                questionAudio={questionAudio}
                questionImage={questionImage}
                answerA={answerA}
                answerB={answerB}
                answerC={answerC}
                answerD={answerD}
                correctAnswer={correctAnswer}
                question2Memo={question2Memo}
                answer2A={answer2A}
                answer2B={answer2B}
                answer2C={answer2C}
                answer2D={answer2D}
                question3Memo={question3Memo}
                answer3A={answer3A}
                answer3B={answer3B}
                answer3C={answer3C}
                answer3D={answer3D}
                guidance={guidance}
                handleChange={handleChange}
                handleOnChangeAnswerQuizz1={(value) => {
                  setState({ correctAnswer: value });
                }}
                handleOnChangeAnswerQuizz2={(value) => {
                  setState({ correct2Answer: value });
                }}
                handleOnChangeAnswerQuizz3={(value) => {
                  setState({ correct3Answer: value });
                }}
              />
            )}
            {type === "PART5" && (
              <QuestionPart5
                questionMemo={questionMemo}
                answerA={answerA}
                answerB={answerB}
                answerC={answerC}
                answerD={answerD}
                correctAnswer={correctAnswer}
                guidance={guidance}
                handleChange={handleChange}
                handleOnChangeAnswerQuizz1={(value) => {
                  setState({ correctAnswer: value });
                }}
              />
            )}
            {type === "PART6" && (
              <QuestionPart6
                questionMemo={questionMemo}
                questionImage={questionImage}
                answerA={answerA}
                answerB={answerB}
                answerC={answerC}
                answerD={answerD}
                correctAnswer={correctAnswer}
                question2Memo={question2Memo}
                answer2A={answer2A}
                answer2B={answer2B}
                answer2C={answer2C}
                answer2D={answer2D}
                correct2Answer={correct2Answer}
                question3Memo={question3Memo}
                answer3A={answer3A}
                answer3B={answer3B}
                answer3C={answer3C}
                answer3D={answer3D}
                correct3Answer={correct3Answer}
                question4Memo={question4Memo}
                answer4A={answer4A}
                answer4B={answer4B}
                answer4C={answer4C}
                answer4D={answer4D}
                correct4Answer={correct4Answer}
                guidance={guidance}
                handleChange={handleChange}
                handleOnChangeAnswerQuizz1={(value) => {
                  setState({ correctAnswer: value });
                }}
                handleOnChangeAnswerQuizz2={(value) => {
                  setState({ correct2Answer: value });
                }}
                handleOnChangeAnswerQuizz3={(value) => {
                  setState({ correct3Answer: value });
                }}
                handleOnChangeAnswerQuizz4={(value) => {
                  setState({ correct4Answer: value });
                }}
              />
            )}
            {type === "PART7" && (
              <QuestionPart7
                questionMemo={questionMemo}
                questionImage={questionImage}
                answerA={answerA}
                answerB={answerB}
                answerC={answerC}
                answerD={answerD}
                correctAnswer={correctAnswer}
                question2Memo={question2Memo}
                answer2A={answer2A}
                answer2B={answer2B}
                answer2C={answer2C}
                answer2D={answer2D}
                correct2Answer={correct2Answer}
                question3Memo={question3Memo}
                answer3A={answer3A}
                answer3B={answer3B}
                answer3C={answer3C}
                answer3D={answer3D}
                correct3Answer={correct3Answer}
                question4Memo={question4Memo}
                answer4A={answer4A}
                answer4B={answer4B}
                answer4C={answer4C}
                answer4D={answer4D}
                correct4Answer={correct4Answer}
                question5Memo={question5Memo}
                answer5A={answer5A}
                answer5B={answer5B}
                answer5C={answer5C}
                answer5D={answer5D}
                correct5Answer={correct5Answer}
                guidance={guidance}
                handleChange={handleChange}
                handleOnChangeAnswerQuizz1={(value) => {
                  setState({ correctAnswer: value });
                }}
                handleOnChangeAnswerQuizz2={(value) => {
                  setState({ correct2Answer: value });
                }}
                handleOnChangeAnswerQuizz3={(value) => {
                  setState({ correct3Answer: value });
                }}
                handleOnChangeAnswerQuizz4={(value) => {
                  setState({ correct4Answer: value });
                }}
                handleOnChangeAnswerQuizz5={(value) => {
                  setState({ correct5Answer: value });
                }}
              />
            )}
          </table>

          <div className="wrap-button">
            <Button type="primary" onClick={handleOnsubmit}>
              Update
            </Button>
            <Button
              onClick={() => {
                history.push("/questions");
              }}
            >
              Cancel
            </Button>
          </div>
        </form>
      </div>
    </WrapStyled>
  );
}

export default UpdateQuestionScreen;
