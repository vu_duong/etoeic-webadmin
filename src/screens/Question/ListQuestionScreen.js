import { FrownOutlined, SmileOutlined } from "@ant-design/icons";
import { Button, notification, Spin, Table, Tabs, Tag, Typography } from "antd";
import Modal from "antd/lib/modal/Modal";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import request from "../../utils/request";

const { TabPane } = Tabs;

const WrapDiv = styled.div`
  &&& {
    padding: 16px;
    .button-create {
      margin-bottom: 16px;
    }
  }
`;

function ManageListQuestionScreen() {
  const columns = [
    {
      title: "Question Id",
      key: "questionId",
      dataIndex: "questionId",
    },
    {
      title: "Question Memo",
      key: "questionMemo",
      dataIndex: "questionMemo",
    },
    {
      title: "Type",
      key: "type",
      dataIndex: "type",
      render: (type) => {
        let color = "lime";
        switch (type) {
          case "PART1":
            color = "lime";
            break;
          case "PART2":
            color = "green";
            break;
          case "PART3":
            color = "cyan";
            break;
          case "PART4":
            color = "blue";
            break;
          case "PART5":
            color = "geekblue";
            break;
          case "PART6":
            color = "purple";
            break;
          case "PART7":
            color = "gold";
            break;
          default:
            color = "grlimeeen";
        }
        return <Tag color={color}>{type}</Tag>;
      },
    },

    {
      title: "Correct Answer",
      key: "correctAnswer",
      dataIndex: "correctAnswer",
      render: (correctAnswer) => (
        <p>
          <strong>{correctAnswer}</strong>
        </p>
      ),
    },
    {
      title: "",
      key: "action",
      dataIndex: "action",
      render: (id) => (
        <div
          style={{
            display: "flex",
          }}
        >
          <Button
            type="primary"
            style={{ marginRight: "4px" }}
            onClick={() => {
              history.push(`/questions/${id}`);
            }}
          >
            Update
          </Button>
          <Button
            type="primary"
            danger
            onClick={() => {
              setSelectedId(id);
              setVisibleModal(true);
            }}
          >
            Delete
          </Button>
        </div>
      ),
    },
  ];

  const history = useHistory();
  const [questionType, setQuestionType] = useState("PART1");
  const [loading, setLoading] = useState(false);
  const [questionData, setQuestionData] = useState([]);

  const [visibleModal, setVisibleModal] = useState(false);
  const [selectedId, setSelectedId] = useState(0);

  const [activeTab, setActiveTab] = useState("PART1");
  const defaultPagination = {
    current: 1,
    pageSize: 5,
    total: 0,
  };

  const [pagination, setPagination] = useState(defaultPagination);

  const handleTabChanged = (key) => {
    setActiveTab(key);
    setPagination(defaultPagination);

    setQuestionType(key);
  };

  const handleTableChange = (newPage, sortEvent) => {
    console.log(newPage, sortEvent);
    fetchListQuestions(newPage);
  };

  const convertQuestionForTable = (rawData) => {
    const data = rawData.questions.map((x) => {
      const correctAnswer = [
        x.correctAnswer,
        x.correct2Answer,
        x.correct3Answer,
        x.correct4Answer,
        x.correct5Answer,
      ]
        .filter(Boolean)
        .join(", ");

      return {
        questionId: x.questionId,
        type: questionType,
        correctAnswer,
        questionMemo: x.questionMemo,
        action: x.questionId,
      };
    });
    setQuestionData(data);
  };

  const fetchListQuestions = async (newPage) => {
    setLoading(true);
    const paramPagination = newPage || pagination;
    setPagination(paramPagination);
    const skip = (paramPagination.current - 1) * paramPagination.pageSize;
    const take = paramPagination.pageSize;

    const options = { type: questionType, skip, take };
    try {
      const questions = await request().get("/admin/question", {
        params: { ...options },
      });
      //const questions = await getListQuestion(options);
      convertQuestionForTable(questions.data);

      const newPagination = { ...paramPagination, total: questions.data.total };
      setPagination(newPagination);
      setLoading(false);
    } catch (error) {
      openErr(error);

      setLoading(false);
    }
  };

  const [loadingFull, setloadingFull] = useState(false);

  const hideModal = () => {
    setVisibleModal(false);
  };

  const onDeleteQuestion = async () => {
    setloadingFull(true);
    try {
      await request().delete(`/admin/question/${selectedId}`);
      openNotification();
      setloadingFull(false);
      setVisibleModal(false);
      setQuestionData(questionData.filter((x) => x.id !== selectedId));
    } catch (error) {
      openErr(error);

      setloadingFull(false);
    }
  };

  const openErr = (error) => {
    notification.open({
      message: "FAILED!",
      description: "Something went wrong, please try again later" + error,
      icon: <FrownOutlined style={{ color: "#ff4d4f" }} />,
      duration: 5,
    });
  };

  const openNotification = () => {
    notification.open({
      message: "Successfully!",
      description: "Delete question successfully!",
      icon: <SmileOutlined style={{ color: "#108ee9" }} />,
      duration: 5,
    });
  };

  useEffect(() => {
    fetchListQuestions();
  }, [questionType]);

  return (
    <WrapDiv>
      {loadingFull && (
        <div className="loading">
          <Spin tip="Loading..." size="large"></Spin>
        </div>
      )}
      <Typography.Title level={3}>Questions</Typography.Title>
      <div className="button-create">
        <Button
          type="primary"
          onClick={() => {
            history.push("questions/new");
          }}
        >
          Create Question
        </Button>
      </div>
      <Tabs activeKey={activeTab} onChange={handleTabChanged}>
        <TabPane tab="PART 1" key="PART1" />
        <TabPane tab="PART 2" key="PART2" />
        <TabPane tab="PART 3" key="PART3" />
        <TabPane tab="PART 4" key="PART4" />
        <TabPane tab="PART 5" key="PART5" />
        <TabPane tab="PART 6" key="PART6" />
        <TabPane tab="PART 7" key="PART7" />
      </Tabs>
      <>
        <Table
          columns={columns}
          dataSource={questionData}
          pagination={pagination}
          loading={loading}
          onChange={handleTableChange}
        ></Table>
      </>
      <Modal
        icon="ExclamationCircleOutlined"
        title="Confirm"
        visible={visibleModal}
        onOk={onDeleteQuestion}
        onCancel={hideModal}
        okType="danger"
        okText="OK"
        cancelText="Cancel"
      >
        <p>Are you sure to disable this user?</p>
      </Modal>
    </WrapDiv>
  );
}

export default ManageListQuestionScreen;
