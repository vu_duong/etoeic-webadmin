import React from "react";
import { Layout } from "antd";
import { Route, Switch } from "react-router-dom";
import DropdownUser from "../components/Navbar/DropdownUser";
import SidebarDemo from "../components/Sidebar/SidebarDemo";
import AdminsScreen from "./AdminsScreen/AdminsScreen";
import CreateBattlesScreen from "./Battles/CreateBattlesScreen";
import ManageBattlesScreen from "./Battles/ManageBattlesScreen";
import ShowBattlesScreen from "./Battles/ShowBattlesScreen";
import DashboardScreen from "./DashboardScreen/DashboardScreen";
import IndexScreen from "./index";
import "./Layout.css";
import CreateUserScreen from "./ManageUsersScreen/CreateUserScreen";
import ManageUsersScreen from "./ManageUsersScreen/ManageUsersScreen";
import UpdateUserScreen from "./ManageUsersScreen/UpdateUserScreen";
import NoFoundPage from "./NotFound";
import CreateQuestionScreen from "./Question/CreateQuestionScreen";
import ListQuestionScreen from "./Question/ListQuestionScreen";
import UpdateQuestionScreen from "./Question/UpdateQuestionScreen";
import ReportScreen from "./Report/ReportScreen";
import ManageToeicTestScreen from "./ToeicTestScreen/ManageToeicTestScreen";
import CreateToeicTestScreen from "./ToeicTestScreen/CreateToeicTestScreen";
import UpdatePackageScreen from "./Battles/UpdatePackageScreen";

const { Header, Content, Footer } = Layout;

function CustomLayout() {
  const user = localStorage.getItem("token");

  if (user) {
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <SidebarDemo></SidebarDemo>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: " 0 35px",
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <DropdownUser />
          </Header>
          <Content style={{ margin: "0 16px" }}>
            <Switch>
              <Route path="/" exact component={IndexScreen} />
              <Route path="/dashboard" component={DashboardScreen} />
              <Route path="/report" exact component={ReportScreen} />

              <Route path="/users" exact component={ManageUsersScreen} />
              <Route path="/users/new" exact component={CreateUserScreen} />
              {/* <Route path="/users/:id" exact component={ShowUserScreen} /> */}
              <Route path="/users/:id" exact component={UpdateUserScreen} />

              <Route path="/questions" exact component={ListQuestionScreen} />

              <Route
                path="/questions/new"
                exact
                component={CreateQuestionScreen}
              />
              <Route
                path="/questions/:id"
                exact
                component={UpdateQuestionScreen}
              />

              <Route path="/packages" exact component={ManageBattlesScreen} />
              <Route
                path="/packages/new"
                exact
                component={CreateBattlesScreen}
              />
              <Route
                path="/packages/:id"
                exact
                component={UpdatePackageScreen}
              />

              <Route
                path="/toeictest"
                exact
                component={ManageToeicTestScreen}
              />
              <Route
                path="/toeictest/new"
                exact
                component={CreateToeicTestScreen}
              />
              <Route
                path="/toeictest/:id"
                exact
                component={ShowBattlesScreen}
              />

              <Route path="/admins" component={AdminsScreen} />
              <Route path="*" component={NoFoundPage} />
            </Switch>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            eTOEIC ©2020 Created by Duong Huy Vu
          </Footer>
        </Layout>
      </Layout>
    );
  } else {
    window.location.assign("/login");
  }
}

export default CustomLayout;
