import { FrownOutlined, SmileOutlined } from "@ant-design/icons";
import { Button, notification, Spin, Table, Tag, Typography } from "antd";
import Modal from "antd/lib/modal/Modal";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import request from "../../utils/request";

const WrapDiv = styled.div`
  &&& {
    padding: 16px;
    .button-create {
      margin-bottom: 16px;
    }
  }
`;

function MangeBattlesScreen() {
  const columns = [
    {
      title: "Id",
      key: "id",
      dataIndex: "id",
    },
    {
      title: "Title",
      key: "title",
      dataIndex: "title",
    },
    {
      title: "Type",
      key: "type",
      dataIndex: "type",
      render: (type) => {
        let color = "lime";
        switch (type) {
          case "PART1":
            color = "lime";
            break;
          case "PART2":
            color = "green";
            break;
          case "PART3":
            color = "cyan";
            break;
          case "PART4":
            color = "blue";
            break;
          case "PART5":
            color = "geekblue";
            break;
          case "PART6":
            color = "purple";
            break;
          case "PART7":
            color = "gold";
            break;
          default:
            color = "grlimeeen";
        }
        return <Tag color={color}>{type}</Tag>;
      },
    },
    {
      title: "Level",
      key: "level",
      dataIndex: "level",
      render: (level) => {
        let color = "#87d068";
        if (level === "MEDIUM") {
          color = "#108ee9";
        }
        if (level === "HARD") color = "#f50";
        return <Tag color={color}>{level}</Tag>;
      },
    },
    {
      title: "Total Quizz",
      key: "numberOfQuizz",
      dataIndex: "numberOfQuizz",
    },
    {
      title: "Reward",
      key: "reward",
      dataIndex: "reward",
    },
    {
      title: "Action",
      key: "action",
      dataIndex: "id",
      render: (id) => (
        <div
          style={{
            display: "flex",
          }}
        >
          <Button
            type="primary"
            style={{ marginRight: "4px" }}
            onClick={() => {
              history.push(`/packages/${id}`);
            }}
          >
            Update
          </Button>
          <Button
            type="primary"
            danger
            onClick={() => {
              setSelectedId(id);
              setVisibleModal(true);
            }}
          >
            Delete
          </Button>
        </div>
      ),
    },
  ];
  const history = useHistory();

  const [loading, setLoading] = useState(false);
  const [packageData, setPackageData] = useState([]);

  const [visibleModal, setVisibleModal] = useState(false);
  const [selectedId, setSelectedId] = useState(0);

  const [loadingFull, setLoadingFull] = useState(false);

  const defaultPagination = {
    current: 1,
    pageSize: 5,
    total: 0,
  };

  const [pagination, setPagination] = useState(defaultPagination);

  const fetchListPackage = async (newPage) => {
    setLoading(true);
    const paramPagination = newPage || pagination;
    setPagination(paramPagination);
    const skip = (paramPagination.current - 1) * paramPagination.pageSize;
    const take = paramPagination.pageSize;

    const options = { skip, take };
    try {
      const packages = await request().get("/admin/package", {
        params: { ...options },
      });
      //const packages = await getListPracticePackage(options);
      console.log(packages.data);

      convertPackageForTable(packages.data.data);

      const newPagination = { ...paramPagination, total: packages.data.total };
      setPagination(newPagination);
    } catch (error) {
      setLoading(false);
    }
    setLoading(false);
  };

  const convertPackageForTable = (rawData) => {
    const data = rawData.map((x) => {
      return {
        ...x,
        action: x.id,
      };
    });
    console.log(data);
    setPackageData(data);
  };

  useEffect(() => {
    fetchListPackage();
  }, []);

  const handleTableChange = (newPage, sortEvent) => {
    console.log(newPage, sortEvent);
    fetchListPackage(newPage);
  };

  const deletePackage = async () => {
    setLoadingFull(true);

    try {
      await request().delete(`/admin/package/${selectedId}`);
      setLoadingFull(false);
      openNotification();

      setPackageData(packageData.filter((x) => x.id !== selectedId));
    } catch (error) {
      openError(error);
      setLoadingFull(false);
    }

    setVisibleModal(false);
    fetchListPackage();
  };

  const hideModal = () => {
    setVisibleModal(false);
  };

  const openNotification = () => {
    notification.open({
      message: "Successfully!",
      description: "Delete Package successfully!",
      icon: <SmileOutlined style={{ color: "#108ee9" }} />,
      duration: 5,
    });
  };

  const openError = (error) => {
    notification.open({
      message: "FAILED!",
      description: "Something went wrong, please try again later" + error,
      icon: <FrownOutlined style={{ color: "#ff4d4f" }} />,
      duration: 5,
    });
  };

  return (
    <WrapDiv>
      {loadingFull && (
        <div className="loading">
          <Spin tip="Loading..." size="large"></Spin>
        </div>
      )}

      <Typography.Title level={3}>Packages</Typography.Title>
      <div className="button-create">
        <Button
          type="primary"
          onClick={() => {
            history.push("packages/new");
          }}
        >
          Create Package
        </Button>
      </div>

      <Table
        columns={columns}
        dataSource={packageData}
        pagination={pagination}
        loading={loading}
        onChange={handleTableChange}
      ></Table>
      <Modal
        icon="ExclamationCircleOutlined"
        title="Confirm"
        visible={visibleModal}
        onOk={deletePackage}
        onCancel={hideModal}
        okType="danger"
        okText="OK"
        cancelText="Cancel"
      >
        <p>Are you sure to delete this package?</p>
      </Modal>
    </WrapDiv>
  );
}

export default MangeBattlesScreen;
