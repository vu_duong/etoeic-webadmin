import { Typography } from "antd";
import React from "react";

function DashboardScreen() {
  return (
    <div>
      <Typography.Title level={3}>Welcome to CMS eTOEIC</Typography.Title>
    </div>
  );
}

export default DashboardScreen;
