import { useReducer } from "react";
import { useHistory } from "react-router-dom";
import request from "../../utils/request";
import "./style.css";

const initialState = {
  username: "",
  password: "",
};

const LoginForm = () => {
  const history = useHistory();
  const [state, setState] = useReducer(
    (preState, newState) => ({ ...preState, ...newState }),
    initialState
  );

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setState({ [name]: value });
  };

  const onLogin = async () => {
    console.log(state);
    const body = { email: state.username, password: state.password };
    try {
      const res = await request().post("/auth/login", body);
      localStorage.setItem("token", res.data);
      history.push("/users");
    } catch (error) {}
  };
  // lam chi co nhay?
  /* zzzzzz */ return (
    <div id="loginform">
      <FormHeader title="Login" />
      <div>
        <div className="row">
          <label>Username</label>
          <input
            type="text"
            placeholder="Enter your username"
            name="username"
            onChange={handleChange}
          />
        </div>

        <div className="row">
          <label>Password</label>
          <input
            type="password"
            placeholder="Enter your password"
            name="password"
            onChange={handleChange}
          />
        </div>
        <br />

        <div id="button" className="row">
          <button onClick={onLogin}>Login</button>
        </div>
      </div>
    </div>
  );
};

const FormHeader = (props) => <h2 id="headerTitle">{props.title}</h2>;

export default LoginForm;
