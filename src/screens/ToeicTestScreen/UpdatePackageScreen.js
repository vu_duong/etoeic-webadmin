import React, { useEffect, useReducer, useState } from "react";
import styled from "styled-components";
import {
  Select,
  Table,
  Transfer,
  Button,
  Input,
  InputNumber,
  Form,
  Typography,
  Spin,
} from "antd";
import * as _ from "lodash";
import { Option } from "antd/lib/mentions";
import request from "../../utils/request";
import { useHistory } from "react-router-dom";

const WrapStyled = styled.div`
  &&& {
    font-size: 14px;
    padding: 16px;
    .wrap-form {
      form {
        display: flex;
        flex-direction: column;
        & > div {
          margin-bottom: 16px;
        }
        .wrap-button {
          display: flex;
          justify-content: center;
          align-items: center;
          button {
            margin: 8px;
          }
        }
      }
    }
  }
`;

const types = [
  {
    id: "PART1",
    name: "Part 1",
  },
  {
    id: "PART2",
    name: "Part 2",
  },
  {
    id: "PART3",
    name: "Part 3",
  },
  {
    id: "PART4",
    name: "Part 4",
  },
  {
    id: "PART5",
    name: "Part 5",
  },
  {
    id: "PART6",
    name: "Part 6",
  },
  {
    id: "PART7",
    name: "Part 7",
  },
];

const leftTableColumns = [
  {
    dataIndex: "questionId",
    title: "Id",
  },
  {
    dataIndex: "questionMemo",
    title: "Question Memo",
  },
  {
    dataIndex: "correctAnswer",
    title: "Answer",
  },
];
const rightTableColumns = [
  {
    dataIndex: "questionId",
    title: "Id",
  },
  {
    dataIndex: "questionMemo",
    title: "Question Memo",
  },
  {
    dataIndex: "correctAnswer",
    title: "Answer",
  },
];

const layout = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 15,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const pageSize = 5;

const UpdatePackageScreen = (props) => {
  const [state, setState] = useReducer(
    (preState, newState) => ({ ...preState, ...newState }),
    {
      name: "",
      description: "",
      type: "PART1",
    }
  );

  const history = useHistory();

  const [dataSource, setDataSource] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);

  const [total, setTotal] = useState(0);

  const handleOnChangeType = (value) => {
    setState({ type: value });
  };

  const { name, description, type } = state;

  const handleOnSubmitPackage = async (value) => {
    setLoading(true);
    try {
      const body = {
        questionIds: targetKeys,
        type,
        ...value,
      };
      await request().post("/admin/package", body);

      setLoading(false);
      // history.push("/questions");
    } catch (error) {
      setLoading(false);
    }
  };

  const [targetKeys, setTargetKeys] = useState([]);

  const onChangeTransfer = (nextTargetKeys, direction, moveKeys) => {
    setTargetKeys(nextTargetKeys);
  };

  const fetchData = async () => {
    setLoading(true);
    const params = {
      type: type,
      skip: (page - 1) * pageSize,
      take: pageSize,
    };
    try {
      const response = await request().get(`/admin/question`, { params });
      setTotal(response.data.total);

      const convert = convertData(response.data.questions);

      setDataSource(convert);
    } catch (error) {
      console.log(error);
    }

    setLoading(false);
  };

  const convertData = (rawData) => {
    return rawData.map((x) => {
      let ans = "";
      if (x.correctAnswer === "A") {
        ans = x.answerA;
      } else if (x.correctAnswer === "B") {
        ans = x.answerB;
      } else if (x.correctAnswer === "C") {
        ans = x.answerC;
      } else {
        ans = x.answerD;
      }
      return {
        key: x.questionId,
        questionId: x.questionId,
        questionMemo: x.questionMemo,
        answerA: x.answerA,
        correctAnswer: ans,
      };
    });
  };

  useEffect(() => {
    fetchData();
  }, [type, page]);

  const onFinish = (value) => {
    handleOnSubmitPackage(value);
  };

  const onPageChange = (value) => {
    setPage(value);
  };
  console.log(type);
  return (
    <WrapStyled>
      {loading && (
        <div className="loading">
          <Spin tip="Loading..." size="large"></Spin>
        </div>
      )}

      <Typography.Title level={3}>Create New Package</Typography.Title>
      <Form
        {...layout}
        name="basic"
        onFinish={onFinish}
        // onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Title"
          name="title"
          rules={[
            {
              required: true,
              message: "Please input title of Package!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Duration"
          name="duration"
          type="number"
          rules={[
            {
              required: true,
              message: "Please input duration of package!",
            },
          ]}
          min={10}
        >
          <InputNumber min={10} />
        </Form.Item>

        <Form.Item
          label="Reward"
          name="reward"
          type="number"
          rules={[
            {
              required: true,
              message: "Please input reward when user completed this package!",
            },
          ]}
        >
          <InputNumber min={100} />
        </Form.Item>

        <Form.Item
          label="Level"
          name="level"
          rules={[
            {
              required: true,
              message: "Please select level of this package!",
            },
          ]}
        >
          <Select>
            <Select.Option value="EASY">Easy</Select.Option>
            <Select.Option value="MEDIUM">Medium</Select.Option>
            <Select.Option value="HARD">Hard</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="Question Type">
          <Select
            value={type}
            style={{ width: 120 }}
            onChange={handleOnChangeType}
          >
            {types.map((ele, index) => {
              return (
                <Option key={index} value={ele.id}>
                  {ele.name}
                </Option>
              );
            })}
          </Select>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 2 }}>
          <Transfer
            dataSource={dataSource}
            targetKeys={targetKeys}
            onChange={onChangeTransfer}
            filterOption={(inputValue, item) =>
              item.questionMemo.indexOf(inputValue) !== -1
            }
            showSelectAll={false}
            showSearch={true}
          >
            {({
              direction,
              filteredItems,
              onItemSelectAll,
              onItemSelect,
              selectedKeys: listSelectedKeys,
              disabled: listDisabled,
            }) => {
              const columns =
                direction === "left" ? leftTableColumns : rightTableColumns;

              const rowSelection = {
                getCheckboxProps: (item) => ({
                  disabled: listDisabled || item.disabled,
                }),
                onSelectAll(selected, selectedRows) {
                  const treeSelectedKeys = selectedRows.map(({ key }) => key);
                  const diffKeys = selected
                    ? _.difference(treeSelectedKeys, listSelectedKeys)
                    : _.difference(listSelectedKeys, treeSelectedKeys);
                  onItemSelectAll(diffKeys, selected);
                },
                onSelect({ key }, selected) {
                  onItemSelect(key, selected);
                },
                selectedRowKeys: listSelectedKeys,
              };

              return (
                <Table
                  rowSelection={rowSelection}
                  loading={loading}
                  columns={columns}
                  dataSource={filteredItems}
                  size="small"
                  style={{ pointerEvents: listDisabled ? "none" : null }}
                  onRow={({ key, disabled: itemDisabled }) => ({
                    onClick: () => {
                      if (itemDisabled || listDisabled) return;
                      onItemSelect(key, !listSelectedKeys.includes(key));
                    },
                  })}
                  pagination={
                    direction === "left"
                      ? { pageSize, total, onChange: onPageChange }
                      : {}
                  }
                />
              );
            }}
          </Transfer>
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button
            type="primary"
            style={{ marginRight: "10px" }}
            htmlType="submit"
          >
            Create
          </Button>
          <Button
            onClick={() => {
              setState({});
            }}
          >
            Cancel
          </Button>
        </Form.Item>
      </Form>
    </WrapStyled>
  );
};

export default UpdatePackageScreen;
