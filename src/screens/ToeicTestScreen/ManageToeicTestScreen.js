import { FrownOutlined, SmileOutlined } from "@ant-design/icons";
import { Button, notification, Spin, Table, Tag, Typography } from "antd";
import Modal from "antd/lib/modal/Modal";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import request from "../../utils/request";

const WrapDiv = styled.div`
  &&& {
    padding: 16px;
    .button-create {
      margin-bottom: 16px;
    }
  }
`;

function ManageToeicTestScreen() {
  const columns = [
    {
      title: "Id",
      key: "id",
      dataIndex: "id",
    },
    {
      title: "Title",
      key: "title",
      dataIndex: "title",
    },
    {
      title: "Type",
      key: "testType",
      dataIndex: "testType",
      render: (type) => {
        const color = type === "FULL" ? "geekblue" : "gold";
        return <Tag color={color}>{type}</Tag>;
      },
    },
    {
      title: "Duration",
      key: "duration",
      dataIndex: "duration",
    },
    {
      title: "Reward",
      key: "totalCoin",
      dataIndex: "totalCoin",
    },
    {
      title: "Action",
      key: "action",
      dataIndex: "id",
      render: (id) => (
        <div
          style={{
            display: "flex",
          }}
        >
          <Button type="primary" style={{ marginRight: "4px" }}>
            Update
          </Button>
          <Button
            type="primary"
            danger
            onClick={() => {
              setSelectedId(id);
              setVisibleModal(true);
            }}
          >
            Delete
          </Button>
        </div>
      ),
    },
  ];
  const history = useHistory();

  const [loading, setLoading] = useState(false);
  const [toeicTestData, setToeicTestData] = useState([]);

  const [visibleModal, setVisibleModal] = useState(false);
  const [selectedId, setSelectedId] = useState(0);

  const [loadingFull, setLoadingFull] = useState(false);

  const defaultPagination = {
    current: 1,
    pageSize: 5,
    total: 0,
  };

  const [pagination, setPagination] = useState(defaultPagination);

  const fetchListToeicTest = async (newPage) => {
    setLoading(true);
    const paramPagination = newPage || pagination;
    setPagination(paramPagination);
    const skip = (paramPagination.current - 1) * paramPagination.pageSize;
    const take = paramPagination.pageSize;

    const options = { skip, take };
    try {
      const toeictest = await request().get("/admin/toeictest", {
        params: { ...options },
      });
      //const packages = await getListPracticePackage(options);

      convertToeicTestForTable(toeictest.data.data);

      const newPagination = { ...paramPagination, total: toeictest.data.total };
      setPagination(newPagination);
    } catch (error) {
      setLoading(false);
    }
    setLoading(false);
  };

  const convertToeicTestForTable = (rawData) => {
    const data = rawData.map((x) => {
      return {
        ...x,
        action: x.id,
      };
    });
    setToeicTestData(data);
  };

  useEffect(() => {
    fetchListToeicTest();
  }, []);

  const handleTableChange = (newPage, sortEvent) => {
    console.log(newPage, sortEvent);
    fetchListToeicTest(newPage);
  };

  const deletePackage = async () => {
    console.log(selectedId);
    setLoadingFull(true);

    try {
      await request().delete(`/admin/package/${selectedId}`);
      setLoadingFull(false);
      openNotification();
    } catch (error) {
      openError(error);
      setLoadingFull(false);
    }

    setVisibleModal(false);
    fetchListToeicTest();
  };

  const hideModal = () => {
    setVisibleModal(false);
  };

  const openNotification = () => {
    notification.open({
      message: "Successfully!",
      description: "Update User successfully!",
      icon: <SmileOutlined style={{ color: "#108ee9" }} />,
      duration: 5,
    });
  };

  const openError = (error) => {
    notification.open({
      message: "FAILED!",
      description: "Something went wrong, please try again later" + error,
      icon: <FrownOutlined style={{ color: "#ff4d4f" }} />,
      duration: 5,
    });
  };

  return (
    <WrapDiv>
      {loadingFull && (
        <div className="loading">
          <Spin tip="Loading..." size="large"></Spin>
        </div>
      )}

      <Typography.Title level={3}>Toeic Test</Typography.Title>
      <div className="button-create">
        <Button
          type="primary"
          onClick={() => {
            history.push("toeictest/new");
          }}
        >
          Create Toeic Test
        </Button>
      </div>

      <Table
        columns={columns}
        dataSource={toeicTestData}
        pagination={pagination}
        loading={loading}
        onChange={handleTableChange}
      ></Table>
      <Modal
        icon="ExclamationCircleOutlined"
        title="Confirm"
        visible={visibleModal}
        onOk={deletePackage}
        onCancel={hideModal}
        okType="danger"
        okText="OK"
        cancelText="Cancel"
      >
        <p>Are you sure to delete this Toeic Test?</p>
      </Modal>
    </WrapDiv>
  );
}

export default ManageToeicTestScreen;
